pokemonModel = require('../model/pokemonModel');

exports.index = function (req, res) {
    pokemonModel.get(function (err, data) {
        if (err)
            res.json({
                status: "error",
                message: err
            });
        res.json({
            status: "success",
            message: "Got pokemon Successfully!",
            data: data
        });
    });
};

exports.add = function (req, res) {
    console.log(req.body);
    var pokemon = new pokemonModel();
    pokemon.name = req.body.name;
    pokemon.url = req.body.url;

    pokemon.save(function (err) {
        if (err)
            res.json(err); res.json({
                message: "New Pokemon Added!",
                data: pokemon
            });
    });
};

exports.view = function (req, res) {
    pokemonModel.findById(req.params.pokemon_id, function (err, data) {
        if (err)
            res.send(err);
        res.json({
            message: 'Pokemon Details',
            data: data
        });
    });
};

exports.update = function (req, res) {
    pokemonModel.findById(req.params.pokemon_id, function (err, pokemon) {
        if (err)
            res.send(err);
        pokemon.name = req.body.name;
        pokemon.url = req.body.url;
        pokemon.save(function (err) {
            if (err)
                res.json(err)
            res.json({
                message: "Pokemon Updated Successfully",
                data: pokemon
            });
        });
    });
};

exports.delete = function (req, res) {
    pokemonModel.deleteOne({
        _id: req.params.pokemon_id
    }, function (err, contact) {
        if (err)
            res.send(err)
        res.json({
            status: "success",
            message: 'Pokemon Deleted'
        })
    })
}