var mongoose = require('mongoose');
var pokemonSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    }
});

var pokemon = module.exports = mongoose.model('pokemon', pokemonSchema); module.exports.get = function (callback, limit) {
    pokemon.find(callback).limit(limit);
}