let router = require('express').Router();

router.get('/', function (req, res) {
    res.json({
        status: 'API',
        message: 'MiniProject API Works'
    });
});

var pokemonController = require('./controllers/pokemonController');// Bio routes

router.route('/pokemon')
    .get(pokemonController.index)
    .post(pokemonController.add);

router.route('/pokemon/:pokemon_id')
    .get(pokemonController.view)
    .patch(pokemonController.update)
    .put(pokemonController.update)
    .delete(pokemonController.delete);

module.exports = router;