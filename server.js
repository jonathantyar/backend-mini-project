const express = require('express');
let mongoose = require('mongoose');
var cors = require('cors')

const app = express();

app.use(cors());

app.listen(8000, function () {
    console.log('Backend server is online!');
});

app.get('/', (req, res) => res.send('Hello World!'))

const MongoDB = 'mongodb+srv://root:145874@miniproject.lqmys.mongodb.net/miniProject?retryWrites=true&w=majority';

const mongo = mongoose.connect(MongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

mongo.then(() => {
    console.log('connected');
}, error => {
    console.log(error, 'error');
})

let apiRoutes = require("./routes")

app.use('/api', apiRoutes)